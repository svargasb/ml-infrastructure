#!/bin/bash
cd ~/ml-python/luigi
source /usr/local/payu/miniconda3/bin/activate mlpython
STARTDATE=$(date +%Y-%m-%d -d -15days)
ENDDATE=$(date +%Y-%m-%d -1day)
python -m datascience RangeDaily --of DownloadDayTask --start $STARTDATE --stop $ENDDATE --workers 5 --task-limit 100 --local-scheduler
