# luigi - Complex pipelines of batch jobs

## Overview

According to the official documentation, 

"The purpose of Luigi is to address all the plumbing typically associated with long-running batch 
processes. You want to chain many tasks, automate them, and failures will happen. 
These tasks can be anything, but are typically long running things like [Hadoop](http://hadoop.apache.org/)
 jobs, dumping data to/from databases, running machine learning algorithms, or anything else.

There are other software packages that focus on lower level aspects of data processing, 
like [Hive](https://hive.apache.org/), [Pig](https://pig.apache.org/), or [Cascading](https://www.cascading.org/)
. [Luigi](https://luigi.readthedocs.io/en/stable/) is not a framework to replace these. 
Instead it helps you stitch many tasks together, where each task can be a [Hive](https://hive.apache.org/)
 query, a [Hadoop](http://hadoop.apache.org/) job in Java,
  a [Spark](https://spark.apache.org/) job in Scala or Python, a Python snippet, 
dumping a table from a database, or anything else. It’s easy to build up long-running pipelines 
that comprise thousands of tasks and take days or weeks to complete. Luigi takes care of a 
lot of the workflow management so that you can focus on the tasks themselves and 
their dependencies."

### Concept

Luigi is built around the concept of [Tasks](http://luigi.readthedocs.io/en/stable/workflows.html#task)
 and [Targets](http://luigi.readthedocs.io/en/stable/workflows.html#target). Tasks define what is needed 
 (`requires` method) to execute a certain task (`run` method) and create some output (`output` method).
 Targets define what is the output of a Task and how you can check if a Task has been successfully
 run or not. With the `requires` method, one defines automatically the Task dependencies of the 
 workflow and in which order it should be run.
For more details, check the workflow documentation [here](http://luigi.readthedocs.io/en/stable/workflows.html).

### Pseudo-Workflow Example

Let us create a simple but common workflow: 

a) Extract data from table A, table B and table C, belonging to database I and database II, respectively

b) Merge data

c) Write merged data to table D in database I

To achieve that:
    
    1.We define a Task to extract from a single table (QueryPostgres)
    2.We define a Task to extract all the necessary data (DownloadDayTask)
    3.We define a Task to join all the data (JoinDailyTask)
    4.We define a Task to upload to a DB (UploadToDB)
    
All the tasks are implemented in the `example.py` file, where we download
    
- maf_transaccion from the maf database
- maf_evaluacion_xml from the maf database
- transaccion from the payu (pol_v4) database

,we merge it all and upload to the `datascience.luigi` table (not necessary to previously create it).

### Run

0. If the necessary conda environment hasn't been created

```bash
conda env create -f environment_filename.yml -n environment_name
```
which in our case could be

```bash
conda env create -f luigienv.yml -n luigienv
```

1. Activate the python environment (Unix)

```bash
conda activate luigienv
```
or (Windows)

```bash
activate luigienv
```

2. Start the luigi daemon
```bash
luigid --port 8892 --logdir .
```

If `--port` is omitted, it serves the [visualizer](http://luigi.readthedocs.io/en/stable/index.html#visualiser-page)
 on port 8082 by default. The visualizer should
now be accesible at [localhost:8082](localhost:8082)

*For more options run* `luigid -h`.


3. Run the task (eg. UploadToDB in module example.py)
```bash
python -m example UploadToDB --date 2018-06-22 --workers 5
```

There are two options to run the Tasks. The first one is the one mentioned above. We start a
scheduler with `luigid` and then when running the task it will connect automatically to the default
scheduler or to the one mentioned in `luigi.cfg`.

For debugging purposes, we do not need to run the `luigid` and we can create a local scheduler with 
the option `--local-scheduler`, that is

```bash
python -m example UploadToDB --date 2018-06-22 --workers 5 --local-scheduler
```

For more info about the `local-scheduler`, see [here](http://luigi.readthedocs.io/en/stable/central_scheduler.html).

## Contents

This folder contains:

- `hello_world.py`

    Do I need to say anything else?

- `datascience.py`

    Contains the tasks currently used in ML
    
- `example.py`

    Example of a possible luigi Task specification
- `luigi`

    Example of a cron job specification
- `daily_tasks.sh`

    Bash script to run the desired tasks
- `luigi.cfg`

    Luigi configuration file
- `logging.cfg`

    Luigi logging configuration file (optional)
- `creds.json`

    DB credentials
- `luigienv.yml`

    Conda environment specification file


And some additional files required by the `datascience.py`

- `CONFIG.py`
- `Concurrent_download.py`


## Examples

#### Hello World

```bash
python -m hello_world HelloWorldTask
```

#### Download Daily Data

```bash
python -m datascience RangeDaily --of DownloadDayTask --start 2018-01-01 --stop 2018-03-18 --workers 20 --task-limit 1000 --days-back 365 --local-scheduler
```

## More info

For more information, check [luigi official documentation](http://luigi.readthedocs.io/en/stable/).