import os
import sys
import re
import json
import csv
from datetime import timedelta, datetime
import glob
from pathlib import Path

import pytz
import pandas as pd
import luigi
import luigi.contrib.postgres
import pyarrow.parquet as pq
from dotenv import load_dotenv
env_path = Path(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))) / '.env'
load_dotenv(dotenv_path=env_path, verbose=True)

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from Concurrent_download import download_day
# from db import get_dtypes
from CONFIG import DB_IP, ENCODING, DB_DIR, DATA_DIR, feature_name_conversion as fnc, datascience_schema

from collections import defaultdict
fallback_dtypes = defaultdict(dict)
fallback_dtypes['maf_transaccion'] = {'bin': 'object',
                    'codigo_comercio': 'object',
                    'transaccion_uuid': 'object',
                    'ip_internacional': 'bool',
                    'tarjeta_internacional': 'bool',
                    'prueba': 'bool',
                    'cliente_id': 'object',
                    'medio_id': 'object',
                    'estado_maf_id': 'object',
                    'tipo_evaluacion_id': 'object',
                    'codigo_comprador': 'object',
                    'email_comprador': 'object',
                    'isp': 'object',
                    'proveedor_correo_gratuito': 'bool'}

# Example
#
# python -m datascience RangeDaily --of DownloadDayTask --start 2018-01-01 --stop 2018-03-18 --workers 20 --task-limit 100 --local-scheduler


class PostgresTable(luigi.Config):
    host = luigi.Parameter(default='localhost')
    password = luigi.Parameter(default=os.environ['POSTGRES_PWD'])
    database = luigi.Parameter(default='postgres')
    user = luigi.Parameter(default=os.environ['POSTGRES_USER'])


class JoinDailyTask(luigi.Task):
    # id = luigi.Parameter()
    date = luigi.DateParameter(default=datetime.now(pytz.utc).date())
    if not os.path.exists(os.path.join(DATA_DIR, 'datascience.transaccion')):
        os.makedirs(os.path.join(DATA_DIR, 'datascience.transaccion'))
    write_path = os.path.join(DATA_DIR, 'datascience.transaccion',
                              'transaccion_{startdate:%Y%m%d}_{enddate:%Y%m%d}.{extension}')

    def requires(self):
        return [DownloadDayTask(date=self.date),
                DownloadDayTask(date=self.date+timedelta(1))]

    def run(self):

        fnames = [Path(i) for i in glob.glob(os.path.join(DATA_DIR, f'*/*{self.date.strftime("%Y%m%d")}_*.parquet'),
                                             recursive=True)]
        d = {}
        for p in fnames:
            d[p.parent.name] = pd.read_parquet(p)
        d['maf.maf_evaluacion_xml1'] = pd.read_parquet(Path(glob.glob(os.path.join(DATA_DIR, 'maf.maf_evaluacion_xml',
                                               f'*{(self.date + timedelta(1)).strftime("%Y%m%d")}_*.parquet'))[0]))
        d['maf.maf_evaluacion_xml'] = pd.concat([d['maf.maf_evaluacion_xml'], d['maf.maf_evaluacion_xml']])
        d['maf.maf_cambio_estado_transaccion'] = d['maf.maf_cambio_estado_transaccion'].groupby(
            'transaccion_uuid').nuevo_estado_id.apply(set)
        pattern = re.compile('[\n]{2,}')
        pattern2 = re.compile('[\s]{2,}')
        d['maf.maf_transaccion'].descripcion = d['maf.maf_transaccion'].descripcion.str.strip()\
            .str.replace(pattern, '\n').str.replace(pattern2, '')
        df = (d['maf.maf_transaccion']
              .merge(d['maf.maf_evaluacion_xml'], on='transaccion_uuid', how='left', suffixes=('_trans', '_eval'))
              # .merge(d['maf.maf_evaluacion_xml1'], on='transaccion_uuid', how='left', suffixes=('_trans', '_eval'))
              .merge(d['maf.maf_passenger_name_record'], on='transaccion_uuid', how='left', suffixes=('', '_pnr'))
              .merge(d['pps.transaccion'], on='transaccion_id', suffixes=('', '_pps'))
              .merge(d['pps.transaccion_datos_extra'], on='transaccion_id')
              .merge(d['pps.orden'], on='orden_id')
              .merge(d['maf.maf_cambio_estado_transaccion'].reset_index(), on='transaccion_uuid')
              )
        df[sorted(df.columns)].to_csv(
            self.write_path.format(startdate=self.date, enddate=self.date + timedelta(1), extension='csv'),
            index=False, encoding=ENCODING, na_rep=r'\N')

        df['nuevo_estado_id'] = df['nuevo_estado_id'].map(str)
        df[sorted(df.columns)].to_parquet(
            self.write_path.format(startdate=self.date, enddate=self.date + timedelta(1), extension='parquet'))

    def output(self, is_tmp=True):
        return [luigi.LocalTarget(self.write_path.format(startdate=self.date, enddate=self.date+timedelta(1),
                                                         extension='parquet')),
                luigi.LocalTarget(self.write_path.format(startdate=self.date, enddate=self.date+timedelta(1),
                                                         extension='csv'))
                ]


class UpdateTransactionsDB(luigi.contrib.postgres.CopyToTable):
    date = luigi.DateParameter(default=datetime.now(pytz.utc).date()-timedelta(1))

    host = PostgresTable(host=DB_IP).host
    password = PostgresTable().password
    database = PostgresTable().database
    user = PostgresTable().user
    table = 'datascience.transaccion'

    @property
    def source_parquet(self):
        return self.input()[0][0].path.format(startdate=self.date, enddate=self.date+timedelta(1))

    @property
    def source_csv(self):
        return self.input()[0][1].path.format(startdate=self.date, enddate=self.date+timedelta(1))

    @property
    def columns(self):
        columns = [(field.name, field.type) for field in pq.read_schema(self.source_parquet)
                   if field.name != '__index_level_0__']
        return columns

    def rows(self):
        with open(self.source_csv, 'r', encoding=ENCODING) as csv_file:
            reader = csv.reader(csv_file)
            header = next(reader)
            for row in reader:
                yield row

    def requires(self):
        return [JoinDailyTask(date=self.date)]


class CleanUp(luigi.WrapperTask):
    date = luigi.DateParameter(default=datetime.now(pytz.utc).date() - timedelta(1))

    def requires(self):
        return [UpdateTransactionsDB(date=self.date)]

    def run(self):
        for fname in glob.glob(os.path.join(DATA_DIR, 'datascience.transaccion', 'transaccion_*')):
            os.remove(fname)


class DownloadFromTable(luigi.Task):
    # id = luigi.Parameter()
    date = luigi.DateParameter(default=datetime.now(pytz.utc).date()-timedelta(1))
    path = luigi.Parameter(default=os.path.join(DATA_DIR, 'maf.maf_transaccion'))
    table = luigi.Parameter(default='maf.maf_transaccion')
    query = luigi.Parameter(default='select * from maf.maf_transaccion where fecha_creacion between %s and %s')

    def run(self):
        with open(os.path.join(DB_DIR, 'creds.json'), 'r') as f:
            if self.table.startswith('maf') or self.table.startswith('jarvis'):
                DSN = "postgres://{user}:{password}@{host}:{port}/{database}".format(**json.load(f)['maf'], password=os.environ['maf_PASSWORD'])
            elif self.table.startswith('pps'):
                DSN = "postgres://{user}:{password}@{host}:{port}/{database}".format(**json.load(f)['payu'],password=os.environ['payu_PASSWORD'])
            elif self.table.startswith('pol'):
                DSN = "postgres://{user}:{password}@{host}:{port}/{database}".format(**json.load(f)['pol'],
                                                                                     password=os.environ['pol_PASSWORD'])
        download_day(self.date, self.date+timedelta(1), self.table, self.query, DSN, self.path)

    def output(self):
        file_path = os.path.join(self.path, '{table}_{startdate}_{enddate}.csv'.format(
            table=self.table.split(".")[1], startdate=self.date.strftime('%Y%m%d'),
            enddate=(self.date+timedelta(1)).strftime('%Y%m%d')
        )
        )
        return luigi.LocalTarget(path=file_path)


class ParseCSV(luigi.Task):
    date = luigi.DateParameter(default=datetime.now(pytz.utc).date() - timedelta(1))
    path = luigi.Parameter(default=os.path.join(DATA_DIR, 'maf.maf_transaccion'))
    schema = luigi.Parameter(default='maf')
    table = luigi.Parameter(default='maf.maf_transaccion')
    query = luigi.Parameter(default='select * from maf.maf_transaccion where fecha_creacion between %s and %s')
    dtypes = luigi.Parameter(default=None)

    def requires(self):
        return [DownloadFromTable(
                    date=self.date,
                    path=self.path,
                    table=self.table,
                    query=self.query
                )]

    def run(self):
        table = str(self.table).split('.')[-1]
        cols = datascience_schema.get(table)
        csv_fpath = os.path.join(self.path, '{table}_{startdate}_{enddate}.csv'.format(
            table=self.table.split(".")[1], startdate=self.date.strftime('%Y%m%d'),
            enddate=(self.date+timedelta(1)).strftime('%Y%m%d')
        )
        )
        parquet_fpath = os.path.join(self.path, '{table}_{startdate}_{enddate}.parquet'.format(
            table=self.table.split(".")[1], startdate=self.date.strftime('%Y%m%d'),
            enddate=(self.date+timedelta(1)).strftime('%Y%m%d')
        )
        )
        if cols is not None:
            if self.dtypes is None:
                self.dtypes = {k: v for k, v in get_dtypes(schema=self.schema, table=table).items()
                               if k in cols and v != 'datetime'}
                datetime_dtypes = [k for k, v in get_dtypes(schema=self.schema, table=table).items()
                                   if k in cols and v == 'datetime']

                # Override cause of NA in int type
                self.dtypes['cuotas'] = 'float64'
            try:
                df = pd.read_csv(csv_fpath, usecols=cols, dtype=self.dtypes, parse_dates=datetime_dtypes)
                df.to_parquet(parquet_fpath, engine='pyarrow')
            except ValueError:
                pd.read_csv(csv_fpath).to_parquet(parquet_fpath, engine='pyarrow')
        else:
            pd.read_csv(csv_fpath, dtype=fallback_dtypes[table]).to_parquet(parquet_fpath, engine='pyarrow')

    def output(self):
        file_path = os.path.join(self.path, '{table}_{startdate}_{enddate}.parquet'.format(
            table=self.table.split(".")[1], startdate=self.date.strftime('%Y%m%d'),
            enddate=(self.date + timedelta(1)).strftime('%Y%m%d')
        )
                                 )
        return luigi.LocalTarget(path=file_path)


class DownloadDayTask(luigi.WrapperTask):
    date = luigi.DateParameter(default=datetime.now(pytz.utc).date()-timedelta(1))

    def requires(self):
        return [ParseCSV(
                    date=self.date,
                    path=os.path.join(DATA_DIR, 'maf.maf_transaccion'),
                    table='maf.maf_transaccion',
                    query='select * from maf.maf_transaccion where fecha_creacion between %s and %s',
                ),
                ParseCSV(
                    date=self.date,
                    path=os.path.join(DATA_DIR, 'maf.maf_evaluacion_xml'),
                    table='maf.maf_evaluacion_xml',
                    query='select * from maf.maf_evaluacion_xml where fecha_creacion between %s and %s'
                ),
                ParseCSV(
                    date=self.date,
                    path=os.path.join(DATA_DIR, 'maf.maf_cambio_estado_transaccion'),
                    table='maf.maf_cambio_estado_transaccion',
                    query='select * from maf.maf_cambio_estado_transaccion where fecha_cambio_estado between %s and %s'
                ),
                # ParseCSV(
                #     data=self.date,
                #     path=os.path.join(DATA_DIR, 'maf.maf_seguimiento_transaccion'),
                #     table='maf.maf_seguimiento_transaccion',
                #     query='select * from maf.maf_seguimiento_transaccion where fecha_creacion between %s and %s'
                # ),
                #ParseCSV(
                #    data=self.date,
                #    path=os.path.join(DATA_DIR, 'maf.maf_observacion_transaccion'),
                #    table='maf.maf_observacion_transaccion',
                #    query='select seguimiento_transaccion_id as transaccion_uuid, observacion, tipo_observacion '
                #          'from maf.maf_observacion_transaccion where fecha_creacion between %s and %s'
                #),
                ParseCSV(
                    date=self.date,
                    path=os.path.join(DATA_DIR, 'maf.maf_passenger_name_record'),
                    table='maf.maf_passenger_name_record',
                    query='select * from maf.maf_passenger_name_record where fecha_creacion between %s and %s'
                ),
                # ParseCSV(
                #     date=self.date,
                #     path=os.path.join(DATA_DIR, 'pps.disputa'),
                #     table='pps.disputa',
                #     query='select * from pps.disputa where fecha_creacion between %s and %s'
                # ),
                ParseCSV(
                    date=self.date,
                    path=os.path.join(DATA_DIR, 'pps.transaccion'),
                    table='pps.transaccion',
                    query='select transaccion_id, direccion_ip, pagador_cliente_pagador_id, session_id, orden_id,'
                          'pagador_direccion_cobro_codigo_postal, pagador_direccion_cobro_pais, usuario_id, '
                          'proveedor_antifraude,fuente, metodo_integracion, modelo_acreditacion, tipo, '
                          'mensaje_respuesta, codigo_respuesta_red, pagador_email, pagador_nombre_completo, '
                          'pagador_telefono_contacto, red_financiera_pagos, pre_maf_respuesta_decision, '
                          'es_intento_pago, fecha_aprobacion_pago, fecha_operacion, fecha_expiracion, fecha_creacion,'
                          'transaccion_padre_id from pps.transaccion '
                          'where fecha_creacion between %s and %s'
                ),
                ParseCSV(
                date=self.date,
                path=os.path.join(DATA_DIR, 'pps.orden'),
                table='pps.orden',
                query='select orden_id, comprador_email, comprador_nombre_completo'
                      ' from pps.orden '
                      'where fecha_creacion between %s and %s'
                ),
                ParseCSV(
                date=self.date,
                path=os.path.join(DATA_DIR, 'pps.transaccion_datos_extra'),
                table='pps.transaccion_datos_extra',
                query='select transaccion_id, mcc '
                      'from pps.transaccion_datos_extra '
                      'where fecha_creacion between %s and %s'
                ),
                ParseCSV(
                date=self.date,
                path=os.path.join(DATA_DIR, 'jarvis.prediction'),
                schema='jarvis',
                table='jarvis.prediction',
                query='select * from jarvis.prediction where prediction_date between %s and %s'
                )
                ]


if __name__ == '__main__':
    luigi.run()

