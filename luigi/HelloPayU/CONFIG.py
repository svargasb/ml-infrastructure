import os
from dotenv import load_dotenv
from pathlib import Path
env_path = Path(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))) / '.env'
load_dotenv(dotenv_path=env_path, verbose=True)


version = '0.3.0'
ENGINE = 'fastparquet'
ENCODING = 'UTF-8'
ENCODING_MAF = 'latin-1'
# TMP_DIR = os.path.abspath('/usr/local/payu/datascience/tmp')
GFDM_DIR = os.path.abspath(os.path.dirname(__file__))
# DATA_DIR = os.path.abspath('/usr/local/payu/datascience')
DATA_DIR = os.path.abspath('d:\jose.campos\ML\ml-python\data')
DB_DIR = os.path.join(os.path.dirname(GFDM_DIR), 'DB/')
DASH_DIR = os.path.join(os.path.dirname(GFDM_DIR), 'Dashboard/')
RND_STATE = 42
N_PROCESSES = 16
DB_IP = '172.18.35.12'
#IP = '172.18.35.12'
IP = '127.0.0.1'
try:
    NTHREADS = os.cpu_count()-1
except:
    import multiprocessing
    NTHREADS = multiprocessing.cpu_count()-1

N_THREADS = os.environ['N_THREADS']
MAX_MEM_H2O = os.environ['MAX_MEM_H2O']
DATE_FORMAT = '%Y-%m-%d'
TIME_FORMAT = '%H:%M:%S'
DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'

data_prefix = 'GFDM-parquet'

feature_name_conversion = {"card_col": "numero",
                           "date_col": "fecha_creacion",
                           "email_col": "email_comprador",
                           "merchant_col": "codigo_comercio",
                           "ip_col": "ip_comprador",
                           "bin_col": "bin",
                           "transaction_id_col": "transaccion_uuid",
                           "amount_col": "valor_COP",
                           "fraud_col": "fraud",
                           "state_col": "estado_maf_id",
                           "country_bin_col": "pais_bin_iso"
                           }

datascience_schema = {'maf_transaccion': ['transaccion_uuid', 'codigo_comercio', 'estado_maf_id', 'valor',
    'fecha_creacion', 'moneda_iso', 'bin', 'prueba', 'ip_internacional', 'tarjeta_internacional',
    'proveedor_correo_gratuito', 'ip_proxy', 'isp', 'ip_comprador', 'email_comprador', 'numero', 'es_contracargo',
    'medio_id', 'cliente_id', 'cuotas', 'pais_bin_iso', 'pais_iso', 'fecha_registro', 'descripcion', 'nombre',
    'nombres_usuario_comprador', 'transaccion_id'],
                      'maf_evaluacion_xml': ['ultima_evaluacion', 'fecha_creacion', 'transaccion_uuid',
    'coincide_lista_negra', 'coincide_lista_blanca', 'tipo_evaluacion_id', 'accion_id'],
                      'maf_informacion_adicional_bin': ['bin', 'marca', 'nivel'],
                      'disputa': ['fecha_creacion', 'transaccion_id', 'estado', 'motivo', 'es_congelado',
    'fecha_notificacion','valor', 'moneda_iso_4217', 'origen', 'aplica_garantia_antifraude', 'ganada_banco'],
                      'maf_cambio_estado_transaccion': ['fecha_cambio_estado', 'nuevo_estado_id', 'transaccion_uuid'],
                      }
