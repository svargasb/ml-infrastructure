from datetime import datetime, timedelta
import json
import csv
import os
from pathlib import Path

import luigi
import pandas as pd
import luigi.contrib.postgres
import psycopg2
import pytz
from dotenv import load_dotenv
env_path = Path(os.path.dirname(os.path.abspath(__file__))) / '.env'
load_dotenv(dotenv_path=env_path, verbose=True)


from luigi.util import inherits

ENCODING = 'utf-8'


class PostgresTable(luigi.Config):
    host = luigi.Parameter(default='localhost')
    password = luigi.Parameter(default=os.environ.get('POSTGRES_PWD'))
    database = luigi.Parameter(default='postgres')
    user = luigi.Parameter(default=os.environ.get('POSTGRES_USER'))


class QueryPostgres(luigi.Task):
    date = luigi.DateParameter(default=datetime.now(pytz.utc).date()-timedelta(1))   # yesterday
    table = luigi.Parameter(default='maf_transaccion')
    query = luigi.Parameter(default='select * from datascience.transaccion '
                            'where %s = fecha_creacion::date '
                            'order by fecha_creacion desc '
                            'limit 1000')
    db = luigi.Parameter(default='maf')

    def output(self):
        return luigi.LocalTarget(path=f'./example_{self.db}_{self.table}_{self.date.strftime("%Y%m%d")}.csv')

    def run(self):
        with open('creds.json', 'r') as f:
            d = json.load(f)[self.db]
        conn = psycopg2.connect(
            dbname=d['database'],
            user=d['user'],
            host=d['host'],
            port=d['port'],
            password=os.environ.get('PG_PASSWORD'))
        cur = conn.cursor()
        cur.execute(self.query, (self.date,))
        columns = [i[0] for i in cur.description]
        rows = cur.fetchall()
        with open(self.output().path, "w", encoding='utf-8') as out_file:
            writer = csv.writer(out_file, lineterminator='\n')
            writer.writerow(columns)
            writer.writerows(rows)


class DownloadDayTask(luigi.WrapperTask):
    date = luigi.DateParameter(default=datetime.now(pytz.utc).date()-timedelta(1))
    # db = luigi.Parameter(default='maf')

    def requires(self):
        return [QueryPostgres(
                    date=self.date,
                    table='maf.maf_transaccion',
                    db='maf',
                    query='select * from maf.maf_transaccion where fecha_creacion::date=%s '
                          'order by fecha_creacion desc limit 10000'
                ),
                QueryPostgres(
                    date=self.date,
                    table='maf_evaluacion_xml',
                    db='maf',
                    query='select * from maf.maf_evaluacion_xml where fecha_creacion::date=%s '
                          'order by fecha_creacion desc limit 10000'
                ),
                QueryPostgres(
                    date=self.date,
                    db='payu',
                    table='transaccion',
                    query='select transaccion_id, direccion_ip, pagador_cliente_pagador_id, session_id, orden_id,'
                          'pagador_direccion_cobro_codigo_postal, pagador_direccion_cobro_pais, usuario_id, '
                          'proveedor_antifraude,fuente, metodo_integracion, modelo_acreditacion, tipo, '
                          'mensaje_respuesta, codigo_respuesta_red, pagador_email, pagador_nombre_completo, '
                          'pagador_telefono_contacto, red_financiera_pagos, pre_maf_respuesta_decision, '
                          'es_intento_pago, fecha_aprobacion_pago, fecha_operacion, fecha_expiracion, fecha_creacion,'
                          'transaccion_padre_id from pps.transaccion '
                          'where fecha_creacion::date = %s order by fecha_creacion desc limit 10000'
                )
                ]

    def run(self):
        self.db = self.input()[0].db


@inherits(DownloadDayTask)
class JoinDailyTask(luigi.Task):
    date = luigi.DateParameter(default=datetime.now(pytz.utc).date()-timedelta(1))
    write_path = './example_merged_{date:%Y%m%d}.csv'

    def requires(self):
        return [DownloadDayTask(date=self.date)]

    def run(self):
        df = {}
        df['trans'] = pd.read_csv(f'example_{self.db}_maf.maf_transaccion_{self.date.strftime("%Y%m%d")}.csv')
        df['eval'] = pd.read_csv(f'example_{self.db}_maf_evaluacion_xml_{self.date.strftime("%Y%m%d")}.csv')
        df['pps.transaccion'] = pd.read_csv(f'example_payu_transaccion_{self.date.strftime("%Y%m%d")}.csv')

        joineddf = (df['trans']
                    .merge(df['eval'], on='transaccion_uuid', how='left', suffixes=('_trans', '_eval'))
                    .merge(df['pps.transaccion'], left_on='transaccion_id_trans', right_on='transaccion_id', suffixes=('', '_pps'))
                    )
        joineddf[sorted(joineddf.columns)].to_csv(
            self.write_path.format(date=self.date), index=False, encoding=ENCODING, na_rep=r'\N')

    def output(self, is_tmp=True):
        return [luigi.LocalTarget(self.write_path.format(date=self.date))
                ]


class UploadToDB(luigi.contrib.postgres.CopyToTable):
    date = luigi.DateParameter(default=datetime.now(pytz.utc).date()-timedelta(1))

    host = PostgresTable().host
    password = PostgresTable().password
    database = PostgresTable().database
    user = PostgresTable().user
    table = 'datascience.luigi'

    @property
    def source_csv(self):
        return self.input()[0][0].path.format(date=self.date)

    @property
    def columns(self):
        with open(self.source_csv, 'r', encoding=ENCODING) as f:
            columns = f.readline().strip().split(',')
        return columns

    def rows(self):
        with open(self.source_csv, 'r', encoding=ENCODING) as csv_file:
            reader = csv.reader(csv_file)
            header = next(reader)
            for row in reader:
                yield row

    def requires(self):
        return [JoinDailyTask(date=self.date)]

if __name__ == '__main__':
    luigi.run()
