# author = Jose Pedro Silva
# Last updated: 29/01/2017
#
# Example:
#   python Concurrent_download.py -s 2017-07-17 -e 2017-07-18 -f D -t maf.maf_evaluacion_xml
#                                 -q "select * from %s where fecha_creacion between %s and %s"

import csv
import time
import datetime as dt
import json
import os
from concurrent import futures
from datetime import datetime
from itertools import repeat

import pandas as pd
import sys
from psycopg2 import connect
from psycopg2.extensions import TransactionRollbackError


def download_day(start_date, end_date, table, query, DSN, dirname):
    with connect(DSN) as conn:
        c = conn.cursor()
        c.execute(query, (start_date, end_date))
        columns = [i[0] for i in c.description]
        rows = c.fetchall()
        start_date_str = start_date.strftime('%Y%m%d')
        end_date_str = end_date.strftime('%Y%m%d')
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        with open(os.path.join(dirname, f'{table.split(".")[1]}_{start_date_str}_{end_date_str}.csv'), 'w',
                  encoding='utf-8') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow(columns)
            writer.writerows(rows)


def download_chunk(offset, limit, table, query, DSN, dirname):
    with connect(DSN) as conn:
        c = conn.cursor()
        c.execute(query, (offset, limit))
        columns = [i[0] for i in c.description]
        rows = c.fetchall()
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        with open(os.path.join(dirname, f'{table.split(".")[1]}_{offset}_{offset+limit}.csv'), 'w',
                  encoding='utf-8') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow(columns)
            writer.writerows(rows)


def download_many(download_one, params_list, table, query, DSN, max_conn, dirname):
    workers = min(max_conn, len(params_list))
    with futures.ThreadPoolExecutor(workers) as executor:
        res = executor.map(download_one, params_list[:-1], params_list[1:], repeat(table), repeat(query), repeat(DSN), repeat(dirname))
    return len(list(res))


def main(DSN, query, table, start_date, end_date, freq, max_conn, dirname, chunksize, *args, **kwargs):
    if start_date is None:
        start_date = datetime(2017, 1, 1)
    if end_date is None:
        end_date = dt.date.today()  # inclusive
    if table is None:
        table = 'maf.maf_transaccion'
    if query is None:
        query = f'select * from {table}'+' where %s <= fecha_creacion and fecha_creacion < %s limit 10'
    else:
        querys = query.split('%s')
        if len(querys) > 1:
            query = '%s'.join([querys[0] + table + querys[1]]+querys[2:])
        else:
            query = querys[0] + table
    if freq is None:
        freq = 'D'
    if max_conn is None:
        max_conn = 10
    if dirname is None:
        dirname = os.path.join(os.path.dirname(os.path.abspath(__file__)), f'{table}')
    if chunksize:
        raise NotImplementedError('Chunksize is not yet implemented.')
    elif len(querys) > 1:
        dates = [d.to_pydatetime() for d in pd.date_range(start=start_date, end=end_date, freq=freq)]
        try:
            download_many(download_day, dates, table, query, DSN, max_conn, dirname)
        except TransactionRollbackError:
            time.sleep(180)
            download_many(download_day, dates, table, query, DSN, max_conn, dirname)
    else:
        download_day()


if __name__ == '__main__':
    import argparse
    from dotenv import load_dotenv
    from pathlib import Path
    env_path = Path.home() / 'ml-python' / '.env'

    load_dotenv(dotenv_path=env_path, verbose=True) 
    parser = argparse.ArgumentParser()
    parser.add_argument('-q', '--query', type=str, help=r'Query eg select * from %%s where %%s <= fecha_creacion and fecha_creacion < %%s')
    parser.add_argument('-s', '--start_date', type=str, help='Start date in format YYYY-MM-DD (default 2017-01-01)')
    parser.add_argument('-e', '--end_date', type=str, help='End date in format YYYY-MM-DD (default today)')
    parser.add_argument('-f', '--freq', type=str, help='Frequency in days eg. 2D for 2 days (default D)')
    parser.add_argument('-m', '--max_conn', type=int, help='Maximum number of connections (default 10)')
    parser.add_argument('-d', '--dirname', type=str, help='Dir path to save data (default ../data)')
    parser.add_argument('-t', '--table', type=str, help='Table name')
    parser.add_argument('-c', '--chunksize', type=str, help='Chunksize')
    parser.add_argument('-db', '--database', type=str, help='Database')

    args = parser.parse_args()

    if args.chunksize and (args.start_date or args.end_date):
        raise ValueError('You can only choose one of chunk mode and date mode.')

    with open(os.path.join(os.path.dirname(__file__),'creds.json')) as f:
        DSN = "postgresql://{user}:{password}@{host}:{port}/{database}".format(**json.load(f)[args.database], password=os.environ.get(f'{args.database}_PASSWORD'))

    main(DSN, **vars(args))
