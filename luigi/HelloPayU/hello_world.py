import luigi

class HelloWorldTask(luigi.Task):
    def run(self):
        print("{task} says: Hello World!".format(task=self.__class__.__name__))

if __name__ == '__main__':
    luigi.run()
